const body = document.querySelector(".body")
const hamburger = document.querySelector(".hamburger");
const mobile_nav = document.querySelector(".mobile_nav");

let isHidden = true;

hamburger.addEventListener('click', () => {
    if (isHidden) {
        mobile_nav.style.display = "block";
    } else {
        mobile_nav.style.display = "none";
    }
    isHidden = !isHidden;
});